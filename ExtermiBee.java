package SpoffyBots;
import robocode.*;
//import java.awt.Color;


/**
 * ExtermiBee - a robot by (your name here)
 */
public class ExtermiBee extends AdvancedRobot {
	private boolean tracking = false;
	private ScannedRobotEvent lastScanOlder;
	private ScannedRobotEvent lastScan;
	private double lastSightedHeading = 0.0d;
	private double shotPower = 2;

	private final int FIRE_MODE_SWITCH_LIMIT = 3;
	private final int DISTANT_SHOT_POWER = 2;
	private final int CLOSE_SHOT_POWER = 3;
	private int consecMisses = 0;
	private boolean leadingTarget = true;
	
	public void run() {
		//setAdjustGunForRobotTurn(true);
		//setAdjustRadarForGunTurn(true);
		//setAdjustRadarForRobotTurn(true);
		while(true) {
			handleMovement();
			handleTracking();
			execute();
		}
	}

	public void handleMovement() {
		setAhead(100);
		if(lastScan != null) {
			setTurnRight(lastScan.getBearing());
		} else {
			setTurnRight(200);
		}
	}


	public void handleTracking() {
		if(!tracking) {
			setTurnRadarRight(-360);
		} else {
			tracking = false;
			//System.out.println("Time: " + getTime());
			//System.out.println("Heading: " + lastSightedHeading);
			//System.out.println("Radar Heading: " + getRadarHeading());
			//System.out.println("Best Turn: " + headingToBestTurn(getRadarHeading(), lastSightedHeading));
			setTurnRadarRight(headingToBestTurn(getRadarHeading(), lastSightedHeading));
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		System.out.println("Start turn: " + getTime());
		tracking = true;
		updateSighting(e);

		//FIRE THE GUNS.
		double reqHeading = lastSightedHeading;
		if(leadingTarget) {
			reqHeading = calcLeadHeading();
		}
		setTurnGunRight(headingToBestTurn(getGunHeading(), reqHeading));
		setFire(shotPower);
		System.out.println("End turn: " + getTime());
	}

	public void changeFireMode() {
		leadingTarget = !leadingTarget;
	}
	
	public void onBulletHit(BulletHitEvent e) {
		consecMisses = 0;
	}

	public void onBulletMissed(BulletMissedEvent e) {
		consecMisses++;
		if(consecMisses >= FIRE_MODE_SWITCH_LIMIT) {
			changeFireMode();
		}
	}
	
	public void updateSighting(ScannedRobotEvent event) {
			lastSightedHeading = bearingToCompass(bearingTo360(event.getBearing()));
			lastScanOlder = lastScan;
			lastScan = event;
			if(event.getDistance() < 100) {
				shotPower = CLOSE_SHOT_POWER;
			} else {
				shotPower = DISTANT_SHOT_POWER;
			}
	}
	
	public double bearingToCompass(double bearing) {
			return (bearing + getHeading())%360;
	}
	
	public double bearingTo360(double bearing) {
			if(bearing < 0) {
					return (360d + bearing);
			}
			return bearing;
	}

	public double headingToBestTurn(double currentHeading, double desiredHeading) {
		double bestHeading = desiredHeading - currentHeading;
		if(Math.abs(bestHeading) > 180d) {
			double tempHeading =  360d - Math.abs(bestHeading);
			//If it was positive, flip sign, as we want it to be the opposite to previously.
			if(bestHeading > 0) {
				tempHeading = tempHeading * -1;
			}
			bestHeading = tempHeading;
		}
		return bestHeading;
	}
	
	public double calcLeadHeading() {
			double xCoord = calcXCoord(lastSightedHeading, lastScan.getDistance());
			double yCoord = calcYCoord(lastSightedHeading, lastScan.getDistance());
			
			//double bulletSpeed = Rules.getBulletSpeed(shotPower);
			double bulletSpeed = 20-(3*shotPower);
			double turn = 0; 
			double targetHeading = lastScan.getHeading();
			double deltaX = 0;
			double deltaY = 0;
			double deltaHeading = 0;

			double expectedX = xCoord + deltaX;
			double expectedY = yCoord + deltaY;

			if(lastScanOlder != null && lastScan.getName().equals(lastScanOlder.getName())) {
				deltaHeading = lastScan.getHeading() - lastScanOlder.getHeading();
			}

			while(turn * bulletSpeed < Math.sqrt(Math.pow(expectedX,2) + Math.pow(expectedY,2))) {
				deltaX += Math.sin(Math.toRadians(targetHeading)) * lastScan.getVelocity();
				deltaY += Math.cos(Math.toRadians(targetHeading)) * lastScan.getVelocity();
				
				targetHeading += deltaHeading;

				expectedX = xCoord + deltaX;
				expectedY = yCoord + deltaY;

				turn++;

			}		
			
			System.out.println("Turn: " + turn);
			
			double heading = xYToHeading(expectedX, expectedY);
			
		 /*	System.out.println("HEADING: " + heading);
			System.out.println("CURRENT:" + getGunHeading());
			System.out.println("Velocity: " + event.getVelocity());
			System.out.println("D: " + event.getDistance());
			System.out.println("ED: " + expectedDistance);
			System.out.println("Time: " + time);
			System.out.println("EX: " + expectedX);
			System.out.println("EY: " + expectedY);
			System.out.println("DX: " + deltaX);
			System.out.println("DY: " + deltaY);
			System.out.println("X: " + xCoord);
			System.out.println("Y: " + yCoord);
			System.out.println("");
			*/
			return heading;
	}
	
	public double calcXCoord(double heading, double distance) {
			return Math.sin(Math.toRadians(heading)) * distance;
	}
	
	public double calcYCoord(double heading, double distance) {
			return Math.cos(Math.toRadians(heading)) * distance;
	}
	
	public double xYToHeading(double x, double y) {
		double angle = Math.toDegrees(Math.atan(Math.abs(y/x)));
		if(x >= 0 && y >= 0) {
			angle = 90d - angle; 
		} else if(x >= 0 && y < 0) {
			angle += 90d;
		} else if(x < 0 && y < 0) {
			angle = 270d - angle;
		} else {	
			angle += 270d;
		}
		
		return angle;	
	}	
}
