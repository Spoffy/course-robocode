package SpoffyBots;
import robocode.*;
//import java.awt.Color;


/**
 * ExtermiBee - a robot by (your name here)
 */
public class ExtermiBeeOld extends AdvancedRobot {
	private boolean tracking = false;
	private ScannedRobotEvent lastScanOlder;
	private ScannedRobotEvent lastScan;
	private double lastSightedHeading = 0.0d;
	private double shotPower = 2;

	private final int FIRE_MODE_SWITCH_LIMIT = 3;
	private final int MINIMUM_SHOT_POWER = 2;
	private int consecMisses = 0;
	private boolean leadingTarget = true;
	
	public void run() {
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);
		while(true) {
			handleMovement();
			handleTracking();
			execute();
		}
	}

	public void handleMovement() {
		setAhead(100);
		if(lastScan != null) {
			setTurnRight(lastScan.getBearing());
		} else {
			setTurnRight(200);
		}
	}


	public void handleTracking() {
		if(!tracking) {
			setTurnRadarRight(-360);
		} else {
			tracking = false;
			System.out.println("Heading: " + lastSightedHeading);
			setTurnRadarRight(headingToBestTurn(getRadarHeading(), lastSightedHeading));
			scan();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		tracking = true;
		updateSighting(e);

		//FIRE THE GUNS.
		System.out.println("Leading: " + leadingTarget);
		double reqHeading = lastSightedHeading;
		if(leadingTarget) {
			reqHeading = calcLeadHeading(e);
		}
		double gunHeading = getGunHeading();
		turnGunRight(headingToBestTurn(gunHeading, reqHeading));
		fire(shotPower);
	}

	public void changeFireMode() {
		leadingTarget = !leadingTarget;
	}
	
	public void onBulletHit(BulletHitEvent e) {
		consecMisses = 0;
	}

	public void onBulletMissed(BulletMissedEvent e) {
		consecMisses++;
		if(consecMisses >= FIRE_MODE_SWITCH_LIMIT) {
			changeFireMode();
		}
	}
	
	public void updateSighting(ScannedRobotEvent event) {
			lastSightedHeading = bearingToCompass(bearingTo360(event.getBearing()));
			lastScan = event;
			shotPower = (1/event.getDistance()) * 300;
			if(shotPower < MINIMUM_SHOT_POWER) {shotPower = MINIMUM_SHOT_POWER;};
			
	}
	
	public double bearingToCompass(double bearing) {
			return (bearing + getHeading())%360;
	}
	
	public double bearingTo360(double bearing) {
			if(bearing < 0) {
					return (360d + bearing);
			}
			return bearing;
	}

	public double headingToBestTurn(double currentHeading, double desiredHeading) {
		double bestHeading = desiredHeading - currentHeading;
		if(Math.abs(bestHeading) > 180d) {
			double tempHeading =  360d - Math.abs(bestHeading);
			//If it was positive, flip sign, as we want it to be the opposite to previously.
			if(bestHeading > 0) {
				tempHeading = tempHeading * -1;
			}
			bestHeading = tempHeading;
		}
		return bestHeading;
	}
	
	public double calcLeadHeading(ScannedRobotEvent event) {
			double xCoord = calcXCoord(lastSightedHeading, event.getDistance());
			double yCoord = calcYCoord(lastSightedHeading, event.getDistance());
			
			double time = event.getDistance()/Rules.getBulletSpeed(shotPower);
			//double time = event.getDistance()/(20 - (3*shotPower));
			double expectedDistance = event.getVelocity() * time;
			double deltaX = calcXCoord(event.getHeading(), expectedDistance);
			double deltaY = calcYCoord(event.getHeading(), expectedDistance);
			
			double expectedX = xCoord + deltaX;
			double expectedY = yCoord + deltaY;
			
			double heading = xYToHeading(expectedX, expectedY);
			
		 /*	System.out.println("HEADING: " + heading);
			System.out.println("CURRENT:" + getGunHeading());
			System.out.println("Velocity: " + event.getVelocity());
			System.out.println("D: " + event.getDistance());
			System.out.println("ED: " + expectedDistance);
			System.out.println("Time: " + time);
			System.out.println("EX: " + expectedX);
			System.out.println("EY: " + expectedY);
			System.out.println("DX: " + deltaX);
			System.out.println("DY: " + deltaY);
			System.out.println("X: " + xCoord);
			System.out.println("Y: " + yCoord);
			System.out.println("");
			*/
			return heading;
	}
	
	public double calcXCoord(double heading, double distance) {
			double angle = 0.0d;
			double xSign = 1d;
			if(heading <= 90) {
					angle = 90 - heading;
			} else if(heading <= 180) {
					angle = heading - 90;
			} else if(heading <= 270) {
					angle = 270 - heading;
					xSign = -1d;
			} else if(heading <= 360) {
					angle = heading - 270;
					xSign = -1d;
			}
			
			return Math.cos(Math.toRadians(angle)) * distance * xSign;
	}
	
	public double calcYCoord(double heading, double distance) {
			double angle = 0.0d;
			double ySign = 1d;
			if(heading <= 90) {
					angle = 90 - heading;
			} else if(heading <= 180) {
					angle = heading - 90;
					ySign = -1d;
			} else if(heading <= 270) {
					angle = 270 - heading;
					ySign = -1d;
			} else if(heading <= 360) {
					angle = heading - 270;
			}
			
			return Math.sin(Math.toRadians(angle)) * distance * ySign;
	}
	
	public double xYToHeading(double x, double y) {
		double angle = Math.toDegrees(Math.atan(Math.abs(y/x)));
		if(x >= 0 && y >= 0) {
			angle = 90d - angle; 
		} else if(x >= 0 && y < 0) {
			angle += 90d;
		} else if(x < 0 && y < 0) {
			angle = 270d - angle;
		} else {	
			angle += 270d;
		}
		
		return angle;	
	}	
}
